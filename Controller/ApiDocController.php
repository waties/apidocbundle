<?php

namespace Waties\ApiDocBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ApiDocController extends Controller
{
    /**
     * @Route("/api/doc")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        return array();
    }

    /**
     * @Route("/api/swagger.json")
     */
    public function swaggerAction(Request $request)
    {
        $config = $this->get("waties.swagger")->swaggerConfiguration();

        return new JsonResponse($config);
    }
}
