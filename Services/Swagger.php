<?php

namespace Waties\ApiDocBundle\Services;

use Doctrine\Common\Persistence\ConnectionRegistry;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Waties\Bundle\ApiRestBundle\Services\Util;

class Swagger
{
    /**
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->configWatiesApiDoc = $container->getParameter('config_waties_api_doc');
        $this->configWatiesApiRest = $container->getParameter('config_waties_api_rest');
        $this->util = $container->get('waties.api_rest.util');
        $this->routes = $this->util->getRoutes();
    }

    public function getManager($controller)
    {
        $serviceName = preg_replace('/^(\w+).(\w+)$/', '$1.manager.$2', $controller);
        return $this->container->get($serviceName);
    }

    public function swaggerConfiguration()
    {
        $config = array_merge([
            "swagger" => "2.0",
            "basePath" => $this->configWatiesApiRest['basePath'],
            "tags" => [
                "Global",
                "Configuration"
            ],
            "paths" => [
                "/search" => [
                    "get" => [
                        "summary" => "",
                        "description" => "",
                        "parameters" => [
                            [
                                "name" => "q",
                                "in" => "query",
                                "description" => "",
                                "required" => false,
                                "type" => "string"
                            ]
                        ],
                        "tags" => [
                            "Global"
                        ]
                    ]
                ],
                "/translations" => [
                    "get" => [
                        "summary" => "",
                        "description" => "",
                        "tags" => [
                            "Global"
                        ]
                    ]
                ],
                "/resource-config/{resource}" => [
                    "get" => [
                        "summary" => "",
                        "description" => "",
                        "tags" => [
                            "Configuration"
                        ],
                        "parameters" => [
                            [
                                "name" => "resource",
                                "in" => "path",
                                "required" => true,
                                "type" => "string"
                            ]
                        ]
                    ]
                ]
            ],
            "definitions" => [
                'InfosPaginated' => [
                    "properties" => [
                        "nb_results" => [
                            "type" => "integer"
                        ],
                        "nb_pages" => [
                            "type" => "integer"
                        ],
                        "per_page" => [
                            "type" => "integer"
                        ],
                        "page" => [
                            "type" => "integer"
                        ]
                    ]
                ],
                "Error" => [
                    "properties" => [
                        "code" => [
                            "type" => "integer"
                        ],
                        "message" => [
                            "type" => "string"
                        ]
                    ]
                ],
                'Count' => [
                    "properties" => [
                        "nb_results" => [
                            "type" => "integer"
                        ]
                    ]
                ],
            ]
        ], $this->configWatiesApiDoc);

        foreach ($this->getResources() as $controller => $data) {
            $manager = $this->getManager($controller);
            $config = $this->addTag($config, Inflector::pluralize($data['resource']));
            $config = $this->addDefinitions($config, $manager, $data['model']);
            $config = $this->addCrud($config, $manager, $data['resource'], $data['model'], $controller);
        }

        return $config;
    }

    /**
     * @return array
     */
    public function getResources()
    {
        $config = $this->configWatiesApiRest;
        $resources = [];
        foreach ($config['resources'] as $controller => $resource) {
            $model = $resource['classes']['model'];
            $modelSplit = explode('\\', $model);
            $resources[$controller] = [
                'model' => $model,
                'resource' => strtolower(end($modelSplit))
            ];
        }

        return $resources;
    }

    /**
     * @param $swagger
     * @param $resourceClassName
     * @return array
     */
    public function addDefinitions($swagger, $manager, $resourceClassName)
    {
        $reflect = new \ReflectionClass($resourceClassName);
        $definitions[$reflect->getShortName()] = [
            "type" => "object",
            "properties" => []
        ];

        $metadata = $manager->getClassMetadata($resourceClassName);
        $fieldMappings = $metadata->fieldMappings;
        $fields = $this->util->getFieldsOfResource($resourceClassName, $manager);

        foreach ($fields as $fieldName => $type) {
            if (!in_array($fieldName, ['id'])) {
                $propertiesSwagger = [];
                switch ($type) {
                    case "id":
                    case "integer":
                        $propertiesSwagger["type"] = "integer";
                        break;
                    case "float":
                        $propertiesSwagger["type"] = "number";
                        break;
                    case "date":
                        $propertiesSwagger["type"] = "string";
                        $propertiesSwagger["format"] = "date-time";
                        break;
                    case "boolean":
                        $propertiesSwagger["type"] = "boolean";
                        break;
                    case "many":
                        $targetDocument = $fieldMappings[$fieldName]["targetDocument"];
                        $targetDocumentSplit = explode('\\', $targetDocument);
                        $propertiesSwagger["schema"] = [
                            "\$ref" => "#/definitions/" . end($targetDocumentSplit)
                        ];
                        break;
                    default:
                        $propertiesSwagger["type"] = "string";
                        break;
                }
                $definitions[$reflect->getShortName()]['properties'][$fieldName] = $propertiesSwagger;
            }
        }
        foreach ($definitions as $name => $definition) {
            $swagger['definitions'][$name] = $definition;
            $swagger['definitions'][$name . 'Paginated'] = [
                "properties" => [
                    "infos" => [
                        "\$ref" => "#/definitions/InfosPaginated"
                    ],
                    "resources" => [
                        "type" => "array",
                        "items" => [
                            "\$ref" => "#/definitions/" . $name
                        ]
                    ]
                ]
            ];
        }
        return $swagger;
    }

    /**
     * @param $swagger
     * @param $pluralResource
     * @return array
     */
    public function addTag($swagger, $pluralResource)
    {
        array_push($swagger['tags'], [
            "name" => ucfirst($pluralResource)
        ]);
        return $swagger;
    }

    /**
     * @param $manager
     * @param $resourceClassName
     * @return array
     */
    public function getFilterParameters($manager, $resourceClassName)
    {
        $parameters = [];

        $fields = $this->util->getFieldsOfResource($resourceClassName, $manager);

        foreach ($fields as $fieldName => $type) {
            if (!in_array($fieldName, ['id'])) {
                $parameter = [
                    "name" => $fieldName,
                    "in" => "query",
                    "description" => "filter by field $fieldName",
                    "required" => false,
                    "type" => "array"
                ];
                array_push($parameters, $parameter);
            }
        }

        return $parameters;
    }

    /**
     * @param $pluralResource
     * @param $resource
     * @return array
     */
    public function getDefaultResponses($pluralResource, $resource)
    {
        $responses = [
            201 => [
                "description" => "Add a new $resource",
                "schema" => [
                    "\$ref" => "#/definitions/" . ucfirst($resource)
                ]
            ],
            206 => [
                "description" => "Finds $pluralResource paginated",
                "schema" => [
                    "\$ref" => "#/definitions/" . ucfirst($resource) . 'Paginated'
                ]
            ],
            404 => [
                "description" => "The $resource with this id doesn't exist",
                "schema" => [
                    "\$ref" => "#/definitions/Error"
                ]
            ],
            406 => [
                "description" => "Format requested not available, accepted format json and xml"
            ]
        ];
        return $responses;
    }

    /**
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $filterParameters
     * @return array
     */
    public function getIndexAction($pluralResource, $resource, $responses, $filterParameters)
    {
        $index = [
            "summary" => "Finds $pluralResource",
            "produces" => [
                "application/json",
                "application/xml"
            ],
            "parameters" => array_merge(
                [
                    [
                        "name" => "fields",
                        "in" => "query",
                        "description" => "The fields that will be displayed",
                        "required" => false,
                        "type" => "string"
                    ],
                    [
                        "name" => "page",
                        "in" => "query",
                        "description" => "Current page",
                        "required" => false,
                        "type" => "number",
                        "format" => "integer"
                    ],
                    [
                        "name" => "per_page",
                        "in" => "query",
                        "description" => "Number of items per page",
                        "required" => false,
                        "type" => "number",
                        "format" => "integer"
                    ],
                    [
                        "name" => "sort",
                        "in" => "query",
                        "description" => "Sorting on attributes separated per semicolon",
                        "required" => false,
                        "type" => "string"
                    ],
                    [
                        "name" => "desc",
                        "in" => "query",
                        "description" => "Default sorting is ascending, contains the names of the attributes that will be sorted in a descending manner",
                        "required" => false,
                        "type" => "string"
                    ]
                ], $filterParameters
            ),
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                200 => [
                    "description" => "Finds $pluralResource",
                    "schema" => [
                        "type" => "array",
                        "items" => [
                            "\$ref" => "#/definitions/" . ucfirst($resource)
                        ]
                    ]
                ],
                206 => $responses[206],
                406 => $responses[406]
            ]
        ];

        return $index;
    }

    /**
     * @param $paths
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $filterParameters
     * @return array
     */
    public function setIndexAction($paths, $pluralResource, $resource, $responses, $filterParameters)
    {
        $paths["/$pluralResource"]["get"] = $this->getIndexAction($pluralResource, $resource, $responses, $filterParameters);

        return $paths;
    }

    /**
     * @param $paths
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $filterParameters
     * @return array
     */
    public function setCountAction($paths, $pluralResource, $resource, $responses, $filterParameters)
    {
        $indexAction = $this->getIndexAction($pluralResource, $resource, $responses, $filterParameters);

        $paths["/$pluralResource/count"] = [
            "get" => array_merge($indexAction, [
                "summary" => "Count $pluralResource",
                "parameters" => $filterParameters,
                "responses" => [
                    200 => [
                        "description" => "Count $pluralResource",
                        "schema" => [
                            "\$ref" => "#/definitions/Count"
                        ]
                    ],
                    406 => $responses[406]
                ]
            ])
        ];

        return $paths;
    }

    /**
     * @param $paths
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $filterParameters
     * @return mixed
     */
    public function setExportAction($paths, $pluralResource, $resource, $responses, $filterParameters)
    {
        $indexAction = $this->getIndexAction($pluralResource, $resource, $responses, $filterParameters);

        $paths["/$pluralResource/export"] = [
            "get" => array_merge($indexAction, [
                "summary" => "Export $pluralResource",
                "parameters" => array_merge(
                    [
                        [
                            "name" => "format",
                            "in" => "query",
                            "description" => "The exported file format",
                            "required" => true,
                            "type" => "string",
                            "enum" => [
                                "csv",
                                "xls",
                                "xlxs"
                            ]
                        ],
                        [
                            "name" => "fields",
                            "in" => "query",
                            "description" => "The fields that will be displayed",
                            "required" => false,
                            "type" => "string"
                        ],
                        [
                            "name" => "sort",
                            "in" => "query",
                            "description" => "Sorting on attributes separated per semicolon",
                            "required" => false,
                            "type" => "string"
                        ],
                        [
                            "name" => "desc",
                            "in" => "query",
                            "description" => "Default sorting is ascending, contains the names of the attributes that will be sorted in a descending manner",
                            "required" => false,
                            "type" => "string"
                        ]
                    ], $filterParameters
                ),
                "responses" => [
                    200 => [
                        "description" => "Export $pluralResource"
                    ],
                    406 => $responses[406]
                ]
            ])
        ];

        return $paths;
    }

    /**
     * @param $paths
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @return array
     */
    public function setCreateAction($paths, $pluralResource, $resource, $responses)
    {
        $paths["/$pluralResource"]["post"] = [
            "summary" => "Add a new $resource",
            "parameters" => [
                [
                    "in" => "body",
                    "name" => "body",
                    "description" => "",
                    "required" => true,
                    "schema" => [
                        "\$ref" => "#/definitions/" . ucfirst($resource)
                    ]
                ]
            ],
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                201 => $responses[201],
                406 => $responses[406]
            ]
        ];

        return $paths;
    }

    /**
     * @param $paths
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $parameters
     * @return array
     */
    public function setShowAction($paths, $pluralResource, $resource, $responses, $parameters)
    {
        $paths["/$pluralResource/{id}"]["get"] = [
            "summary" => "Find $resource by id",
            "description" => "Returns a single " . $resource,
            "parameters" => [
                array_merge($parameters['id'], ["description" => "id of $resource that needs to be fetched"])
            ],
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                200 => [
                    "description" => "Find $resource by id",
                    "schema" => [
                        "\$ref" => "#/definitions/" . ucfirst($resource)
                    ]
                ],
                404 => $responses[404],
                406 => $responses[406]
            ]
        ];

        return $paths;
    }

    /**
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $parameters
     * @return array
     */
    public function getUpdateAction($pluralResource, $resource, $responses, $parameters)
    {
        $update = [
            "summary" => "Update an existing $resource",
            "parameters" => [
                array_merge($parameters['id'], ["description" => "id $resource to update"]),
                [
                    "in" => "body",
                    "name" => "body",
                    "description" => "",
                    "required" => true,
                    "schema" => [
                        "\$ref" => "#/definitions/" . ucfirst($resource)
                    ]
                ]
            ],
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                200 => [
                    "description" => "Update an existing $resource",
                    "schema" => [
                        "\$ref" => "#/definitions/" . ucfirst($resource)
                    ]
                ],
                404 => $responses[404],
                406 => $responses[406]
            ]
        ];

        return $update;
    }

    /**
     * @param $paths
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $parameters
     * @return mixed
     */
    public function setUpdateAction($paths, $pluralResource, $resource, $responses, $parameters)
    {
        $paths["/$pluralResource/{id}"]["put"] = $this->getUpdateAction($pluralResource, $resource, $responses, $parameters);

        return $paths;
    }

    /**
     * @param $paths
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $parameters
     * @return mixed
     */
    public function setPartialUpdateAction($paths, $pluralResource, $resource, $responses, $parameters)
    {
        $update = $this->getUpdateAction($pluralResource, $resource, $responses, $parameters);

        $paths["/$pluralResource/{id}"]["patch"] = array_merge($update, [
            "summary" => "Partial update an existing $resource"
        ]);

        return $paths;
    }


    /**
     * @param $paths
     * @param $pluralResource
     * @param $resource
     * @param $responses
     * @param $parameters
     * @return array
     */
    public function setDeleteAction($paths, $pluralResource, $resource, $responses, $parameters)
    {
        $paths["/$pluralResource/{id}"]["delete"] = [
            "summary" => "Delete a $resource",
            "parameters" => [
                array_merge($parameters['id'], ["description" => "id $resource to delete"])
            ],
            "tags" => [
                ucfirst($pluralResource)
            ],
            "responses" => [
                204 => [
                    "description" => "Delete a $resource"
                ],
                404 => $responses[404],
                406 => $responses[406]
            ]
        ];

        return $paths;
    }

    /**
     * @param $swagger
     * @param $manager
     * @param $resource
     * @param $model
     * @param $controller
     * @return mixed
     */
    public function addCrud($swagger, $manager, $resource, $model, $controller)
    {
        $configRoute = $this->routes[$controller];

        $pluralResource = Inflector::pluralize($resource);

        $filterParameters = $this->getFilterParameters($manager, $model);

        $parameters = [
            'id' => [
                "name" => "id",
                "in" => "path",
                "required" => true,
                "type" => "string"
            ]
        ];

        $responses = $this->getDefaultResponses($pluralResource, $resource);

        $paths = [];
        $rootPath = '/' . $this->configWatiesApiRest['basePath'] . '/';

        $diff = array_diff($configRoute['actions'], ['index', 'create', 'count', 'export']);
        if (empty($diff)) {
            $paths["/$pluralResource"] = [];
        }

        //Index
        if (in_array('index', $configRoute['actions'])) {
            $paths = $this->setIndexAction($paths, $pluralResource, $resource, $responses, $filterParameters);
        }

        //Create
        if (in_array('create', $configRoute['actions'])) {
            $paths = $this->setCreateAction($paths, $pluralResource, $resource, $responses);
        }

        //Count
        if (in_array('count', $configRoute['actions'])) {
            $paths = $this->setCountAction($paths, $pluralResource, $resource, $responses, $filterParameters);
        }

        //Export
        if (in_array('export', $configRoute['actions'])) {
            $paths = $this->setExportAction($paths, $pluralResource, $resource, $responses, $filterParameters);
        }

        $diff = array_diff($configRoute['actions'], ['show', 'update', 'partial_update', 'delete']);
        if (empty($diff)) {
            $paths["/$pluralResource/{id}"] = [];
        }

        // Show
        if (in_array('show', $configRoute['actions'])) {
            $paths = $this->setShowAction($paths, $pluralResource, $resource, $responses, $parameters);
        }

        //Update
        if (in_array('update', $configRoute['actions'])) {
            $paths = $this->setUpdateAction($paths, $pluralResource, $resource, $responses, $parameters);
        }


        //Partial Update
        if (in_array('partial_update', $configRoute['actions'])) {
            $paths = $this->setPartialUpdateAction($paths, $pluralResource, $resource, $responses, $parameters);
        }

        //Delete
        if (in_array('delete', $configRoute['actions'])) {
            $paths = $this->setDeleteAction($paths, $pluralResource, $resource, $responses, $parameters);
        }

        foreach ($paths as $path => $methods) {
            $swagger['paths'][$path] = $methods;
        }
        return $swagger;
    }
}