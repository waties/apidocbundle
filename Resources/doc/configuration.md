2. Configuring the Documentation
==================

2.1. Configuration
-------------------------------

To begin with, configure WatiesApiDocBundle in your ``app/config.yml`` (or in an imported configuration file) :

```yaml
waties_api_doc:
    host: localhost:8000
    schemes: [http]
```
2.2. Routing
--------------

In your ``app/routing.yml`` (or in an imported routing file), you need to define which route you want to use :

```yaml
waties_api_doc:
    resource: "@WatiesApiDocBundle/Controller/"
    type:     annotation
    prefix:   /
```