WatiesApiDocBundle
==================

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/3e944694-0d3c-466b-b21f-fa4c8109eb4a/mini.png)](https://insight.sensiolabs.com/projects/3e944694-0d3c-466b-b21f-fa4c8109eb4a)

WatiesApiDocBundle is complementary to WatiesApiRestBundle, this bundle allows you to generate a documentation for your APIs.

Official documentation
---------------------------

1. [Installation](Resources/doc/installation.md)
	1. [Adding required bundles to the kernel](Resources/doc/installation.md#adding-required-bundles-to-the-kernel)
1. [Configuring the Documentation](Resources/doc/configuration.md#configuring-the-documentation)
	1. [Configuration](Resources/doc/configuration.md#configuration)
	1. [Routing](Resources/doc/configuration.md#routing)