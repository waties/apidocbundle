<?php

namespace Waties\ApiDocBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('waties_api_doc');

        $rootNode
            ->children()
                ->arrayNode("info")
                    ->children()
                        ->scalarNode('description')->end()
                        ->scalarNode("version")->defaultValue("1.0.0")->end()
                        ->scalarNode("title")->defaultValue("api")->end()
                    ->end()
                ->end()
                ->scalarNode('host')->defaultValue("localhost")->end()
                ->variableNode('schemes')
                    ->defaultValue(['https'])
                ->end()
            ->end();

        return $treeBuilder;
    }
}
